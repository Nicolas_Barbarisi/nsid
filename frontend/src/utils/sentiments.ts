function getPolarityDescription(polarity: number) {
  if (polarity > 0.5) return "Highly Positive";
  if (polarity > 0) return "Somewhat Positive";
  if (polarity === 0) return "Neutral";
  if (polarity > -0.5) return "Somewhat Negative";
  return "Highly Negative";
}

function getSubjectivityDescription(subjectivity: number) {
  if (subjectivity > 0.7) return "Highly Subjective";
  if (subjectivity > 0.3) return "Somewhat Subjective";
  return "Mostly Objective";
}

export { getPolarityDescription, getSubjectivityDescription };
