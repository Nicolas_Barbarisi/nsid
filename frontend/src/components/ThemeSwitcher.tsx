import { useTheme } from '../context/ThemeProvider';

const ThemeSwitcher = () => {
    const { darkTheme, toggleTheme } = useTheme();

    return (
        <div className="flex justify-end p-4">
            <button
                className="px-4 py-2 bg-gray-200 dark:bg-gray-800 rounded-lg shadow-md text-gray-800 dark:text-gray-200"
                onClick={toggleTheme}>
                {darkTheme ? 'Light Mode' : 'Dark Mode'}
            </button>
        </div>
    );
};

export default ThemeSwitcher;
