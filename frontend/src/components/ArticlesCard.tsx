import { ArticleCardProps } from '../types/articles';
import ProgressBar from './ProgressBar';

const ArticleCard: React.FC<ArticleCardProps> = ({ article }) => (
  <li className="bg-white dark:bg-gray-800 rounded-lg shadow-lg overflow-hidden flex flex-col justify-between">
    <a href={article.link} target="_blank" rel="noopener noreferrer" className="block p-6 hover:bg-gray-100 dark:hover:bg-gray-700 transition duration-300 ease-in-out">
      <h2 className="text-xl font-semibold text-blue-600 dark:text-blue-400">{article.title}</h2>
      <p className="text-gray-800 dark:text-gray-200 mt-2">{article.description}</p>
      <p className="text-gray-600 dark:text-gray-400 text-sm">{article.date}</p>
      <p className="text-green-600 dark:text-green-400 text-sm font-medium">{article.topic}</p>
    </a>
    <div className="px-6 py-4 bg-gray-100 dark:bg-gray-700">
      <ProgressBar value={article.sentiment.polarity} label="Polarity" isPolarity />
      <ProgressBar value={article.sentiment.subjectivity} label="Subjectivity" />
    </div>
  </li>
);

export default ArticleCard;