const LoadingSpinner: React.FC = () => (
  <div className="text-center mt-4 bg-white dark:bg-gray-800 rounded-lg shadow-lg overflow-hidden p-4">
    <svg className="animate-spin h-5 w-5 text-blue-600 dark:text-blue-400 mx-auto" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
      <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
      <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V4a10 10 0 00-10 10h2zm2 8a8 8 0 018-8h2a10 10 0 00-10 10v-2z"></path>
    </svg>
  </div>
);

export default LoadingSpinner;