import { Bubble } from 'react-chartjs-2';
import { Chart as ChartJS, Tooltip, Legend, BubbleController, CategoryScale, LinearScale, PointElement } from 'chart.js';
import { Article } from '../types/articles';

ChartJS.register(Tooltip, Legend, BubbleController, CategoryScale, LinearScale, PointElement);

const BubbleChart = ({ articles }: { articles: Article[] }) => {
  const chartData = {
    datasets: [{
      label: 'Article Sentiments',
      data: articles.map((article: Article) => ({
        x: article.sentiment.polarity,
        y: article.sentiment.subjectivity,
        r: 5 * 1, // Set the radius of the bubble
        articleTitle: article.title // Store the title in the data point
      })),
      backgroundColor: 'rgba(255, 99, 132, 0.2)'
    }]
  };

  const chartOptions = {
    scales: {
      x: {
        beginAtZero: true,
        title: {
          display: true,
          text: 'Polarity'
        }
      },
      y: {
        beginAtZero: true,
        title: {
          display: true,
          text: 'Subjectivity'
        }
      }
    },
    plugins: {
      tooltip: {
        callbacks: {
          label: function(context: any) {
            const dataPoint = context.raw;
            const label = dataPoint.articleTitle || 'No title';
            const polarity = dataPoint.x.toFixed(2);
            const subjectivity = dataPoint.y.toFixed(2);
            return `${label} (Polarity: ${polarity}, Subjectivity: ${subjectivity})`;
          }
        }
      },
      legend: {
        display: false // Hide legend if not necessary
      }
    }
  };

  return <Bubble data={chartData} options={chartOptions} />;
};

export default BubbleChart;
