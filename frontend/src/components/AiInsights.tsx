import ProgressBar from './ProgressBar';

interface AiInsightsProps {
  htmlContent: string;
  averagePolarity: number;
  averageSubjectivity: number;
}

const AiInsights: React.FC<AiInsightsProps> = ({ htmlContent, averagePolarity, averageSubjectivity }) => {
  return (
    <div className="bg-white dark:bg-gray-800 rounded-lg shadow-lg overflow-hidden p-4 mt-4">
      <div className="text-gray-800 dark:text-gray-200" dangerouslySetInnerHTML={{ __html: htmlContent }}></div>
      <div className="mt-4">
        <ProgressBar value={averagePolarity} label="Average Polarity" isPolarity />
        <ProgressBar value={averageSubjectivity} label="Average Subjectivity" />
      </div>
    </div>
  );
};

export default AiInsights;
