interface ProgressBarProps {
  value: number;
  label: string;
  isPolarity?: boolean;
}

const ProgressBar: React.FC<ProgressBarProps> = ({ value, label, isPolarity = false }) => {
  const percentage = isPolarity ? ((value + 1) / 2) * 100 : value * 100;

  const progressBarColor = value >= 0.5 ? 'bg-green-500' :
                           value >= 0.25 ? 'bg-yellow-500' : 'bg-red-500';

  return (
      <div className="w-full bg-gray-200 rounded-full dark:bg-gray-700">
          <div style={{ width: `${percentage}%` }}
               className={`text-xs font-medium text-white text-center p-0.5 leading-none rounded-full ${progressBarColor}`}>
              {label}: {value.toFixed(2)}
          </div>
      </div>
  );
};

export default ProgressBar;