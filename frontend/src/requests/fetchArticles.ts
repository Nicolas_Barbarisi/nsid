import { Article } from '../types/articles';

export const fetchArticles = async (
  setArticles: (articles: Article[]) => void,
  setError: (error: string) => void
): Promise<void> => {
  try {
    const response = await fetch(import.meta.env.VITE_API_URL + '/api/articles');
    const data: { articles: Article[] } = await response.json();
    setArticles(data.articles);
  } catch (error) {
    setError('An error occurred while fetching the articles');
  }
};