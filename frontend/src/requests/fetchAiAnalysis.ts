import { AiAnalysis } from '../types/aiAnalysis';
import MarkdownIt from 'markdown-it';

const md = new MarkdownIt();

export const fetchAiAnalysis = async (
  setHtmlContent: (content: string) => void,
  setAiAnalysis: (analysis: AiAnalysis) => void,
  setLoading: (loading: boolean) => void,
  setError: (error: string) => void
): Promise<void> => {
  try {
    const response = await fetch(import.meta.env.VITE_API_URL + '/api/sentiment');
    const data: AiAnalysis = await response.json();
    const htmlContent = md.render(data.insights);
    setHtmlContent(htmlContent);
    setAiAnalysis(data);
  } catch (error) {
    setError('An error occurred while fetching the AI analysis');
  } finally {
    setLoading(false);
  }
};
