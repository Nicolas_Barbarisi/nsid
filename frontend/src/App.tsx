import { useEffect, useState } from 'react';
import ThemeSwitcher from './components/ThemeSwitcher';
import BubbleChart from './components/SentimentChart';
import LoadingSpinner from './components/LoadingSpinner';
import ArticleCard from './components/ArticlesCard';
import AiInsights from './components/AiInsights';
import { fetchAiAnalysis } from './requests/fetchAiAnalysis';
import { fetchArticles } from './requests/fetchArticles';
import { Article } from './types/articles';
import { AiAnalysis } from './types/aiAnalysis';

function App() {
  const [articles, setArticles] = useState<Article[]>([]);
  const [aiAnalysis, setAiAnalysis] = useState<AiAnalysis | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [htmlContent, setHtmlContent] = useState<string>('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        await Promise.all([
          fetchArticles(setArticles, setError),
          fetchAiAnalysis(setHtmlContent, setAiAnalysis, setLoading, setError)
        ]);
        setLoading(false);
      } catch (err) {
        setError('An error occurred while fetching the data');
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="bg-gray-100 dark:bg-gray-900 min-h-screen transition duration-300 ease-in-out">
      <div className="max-w-4xl mx-auto px-4 py-6">
        <ThemeSwitcher />
        <h1 className="text-3xl font-bold text-center my-6 text-blue-600 dark:text-blue-400">News Sentiment Intelligence Dashboard</h1>
        {error && <div className="text-red-500 text-center">{error}</div>}
        {!error && (
          <>
            <p className="text-gray-600 dark:text-gray-400 text-center mb-6">Total articles: {articles.length}</p>
            <BubbleChart articles={articles} />
            <h2 className="text-2xl font-semibold text-blue-600 dark:text-blue-400 mt-6">AI Insights</h2>
            {loading && <LoadingSpinner />}
            {aiAnalysis && (
              <AiInsights
                htmlContent={htmlContent}
                averagePolarity={aiAnalysis.average_polarity}
                averageSubjectivity={aiAnalysis.average_subjectivity}
              />
            )}
            <ul className="grid gap-4 p-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 dark:text-white">
              {articles.map((article, index) => (
                <ArticleCard key={index} article={article} />
              ))}
            </ul>
          </>
        )}
      </div>
    </div>
  );
}

export default App;