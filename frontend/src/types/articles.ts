export interface Article {
  title: string;
  description: string;
  date: string;
  topic: string;
  link: string;
  sentiment: {
    polarity: number;
    subjectivity: number;
  };
}

export interface ArticleCardProps {
  article: Article;
}
