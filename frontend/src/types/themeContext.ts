export type ThemeContextType = {
  darkTheme: boolean;
  toggleTheme: () => void;
};
