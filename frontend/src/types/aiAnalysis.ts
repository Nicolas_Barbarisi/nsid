export interface AiAnalysis {
  average_polarity: number;
  average_subjectivity: number;
  insights: string;
}

export interface AiInsightsProps {
  htmlContent: string;
  averagePolarity: number;
  averageSubjectivity: number;
}
