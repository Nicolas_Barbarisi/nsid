from app import app
import urllib.robotparser
import requests
from flask_cors import CORS
from dotenv import load_dotenv

load_dotenv()

def check_robots_txt(url):
    # Complétez l'URL pour pointer vers le fichier robots.txt
    robots_url = f"{url}/robots.txt"
    
    # Utiliser requests pour récupérer le contenu du fichier robots.txt
    try:
        response = requests.get(robots_url)
        response.raise_for_status()  # Lèvera une exception pour les réponses 4xx/5xx
        robots_txt = response.text
    except requests.RequestException as e:
        print(f"Erreur lors de la récupération de {robots_url}: {e}")
        return
    
    # Utiliser urllib.robotparser pour analyser le contenu
    rp = urllib.robotparser.RobotFileParser()
    rp.parse(robots_txt.splitlines())  # Parse le texte de robots.txt

    # Vérifier si le scraping est autorisé pour une certaine URL
    target_url = f"{url}/path"
    user_agent = 'MyWebScraper'  # Le nom de votre bot/scraper
    can_fetch = rp.can_fetch(user_agent, target_url)

    print(f"Peut accéder à {target_url} : {can_fetch}")

if __name__ == '__main__':
    check_robots_txt("https://www.bbc.com")
    app.run(debug=False)
