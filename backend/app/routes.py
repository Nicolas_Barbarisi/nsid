from app.articles import fetch_articles
from app.sentiments import analyze_sentiment
from app.ia_prompt import get_insights_from_ai
from . import app
from flask import jsonify
from flask_cors import cross_origin
import os

def fetch_and_process_articles():
    articles_data = fetch_articles(os.getenv("TARGET_URL"))
    results = []
    total_polarity = 0
    total_subjectivity = 0
    article_count = 0

    for title, link, description, date, topic in articles_data:
        text = title + (": " + description if description else "")
        sentiment = analyze_sentiment(text)
        results.append({
            "title": title,
            "link": link,
            "description": description,
            "date": date,
            "topic": topic,
            "sentiment": sentiment
        })
        total_polarity += sentiment['polarity']
        total_subjectivity += sentiment['subjectivity']
        article_count += 1

    return total_polarity, total_subjectivity, article_count, results

@app.route('/api/articles')
def articles():
    total_polarity, total_subjectivity, article_count, results = fetch_and_process_articles()

    return jsonify({
        "article_count": article_count,
        "articles": results
    })

@app.route('/api/sentiment')
def sentiment():
    total_polarity, total_subjectivity, article_count, _ = fetch_and_process_articles()
    average_polarity = total_polarity / article_count if article_count else 0
    average_subjectivity = total_subjectivity / article_count if article_count else 0

    # Return the insights obtained from AI
    insights = get_insights_from_ai(average_polarity, average_subjectivity)
    
    return jsonify({
        "average_polarity": average_polarity,
        "average_subjectivity": average_subjectivity,
        "insights": insights
    })

@app.route('/api')
def home():
    return "Hello, World!"
