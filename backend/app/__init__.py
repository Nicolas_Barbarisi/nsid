import os
from flask import Flask
from flask_cors import CORS

app = Flask(__name__)

client_url = os.getenv("CLIENT_URL")
cors = CORS(app, resources={r"/*": {"origins": client_url}})

from . import routes
