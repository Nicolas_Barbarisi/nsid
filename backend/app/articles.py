import requests
from bs4 import BeautifulSoup
import os

def fetch_articles(url):
    try:
        response = requests.get(url, timeout=10)  # Set a timeout for the request
        response.raise_for_status()  # Raise an exception for HTTP errors
    except requests.RequestException as e:
        print(f"Failed to retrieve data: {e}")
        return []

    soup = BeautifulSoup(response.content, 'html.parser')

    articles = []
    seen_links = set()  # Set to track seen article links

    for item in soup.find_all('a', class_='sc-2e6baa30-0 gILusN'):
        # Check if the item or any of its parents has the data-testid you want to exclude
        if item.find_parent(attrs={"data-testid": "illinois-section-10"}) or item.parent.name == 'script' or item.find_parent(class_='gugNoq'):
            continue  # Skip this item

        if item.find('button') or 'British Broadcasting Corporation' in item.get_text(strip=True):
            continue  # Skip items with a button or containing specific text

        link = item['href']
        link = os.getenv("TARGET_URL") + link if not link.startswith('https://') else link

        # Check if the link has already been seen
        if link in seen_links:
            continue  # Skip duplicate article
        seen_links.add(link)  # Add the link to the set of seen links

        title_tag = item.find('h2')
        title = title_tag.get_text(strip=True) if title_tag else item.get_text(strip=True)

        description_tag = item.find('p', attrs={'data-testid': 'card-description'})
        description = description_tag.get_text(strip=True) if description_tag else None

        date_tag = item.find('span', attrs={'data-testid': 'card-metadata-lastupdated'})
        date = date_tag.get_text(strip=True) if date_tag else None

        topic_tag = item.find('span', attrs={'data-testid': 'card-metadata-tag'})
        topic = topic_tag.get_text(strip=True) if topic_tag else None

        articles.append((title, link, description, date, topic))

    return articles
