from textblob import TextBlob

def analyze_sentiment(text):
    testimonial = TextBlob(text)
    return {
        "polarity": testimonial.sentiment.polarity,
        "subjectivity": testimonial.sentiment.subjectivity
    }
