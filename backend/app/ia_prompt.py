import requests
from openai import OpenAI
import os

def get_insights_from_ai(polarity, subjectivity):
  client = OpenAI(
    api_key=os.getenv("OPENAI_API_KEY")
  )

  completion = client.chat.completions.create(
    model="gpt-3.5-turbo",
    messages=[
      {"role": "system", "content": "Propose une analyse détaillée et un aperçu des données sur le sentiment tirées des dernières nouvelles de la BBC et des scores de polarité et de subjectivité fournis. Explique les implications des scores et suggérer les impacts potentiels sur la réception du contenu. Utilise un langage simple et clair pour expliquer les concepts. Parle en français."},
      {"role": "user", "content": "La polarité moyenne des contenus est " + str(polarity) + " et la subjectivité moyenne est de " + str(subjectivity) + "."}

    ]
  )
  print(completion.choices[0].message.content)
  return completion.choices[0].message.content
